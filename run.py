from flask import Flask, render_template, jsonify
import json
import csv
app = Flask(__name__)

# Read input file and return the values as a dictionary
def readFileAndReturnDictionary(file):	
	allKeys = {}
	CSVdict = {}
	with open(file, mode='r') as infile:
		reader = csv.reader(infile)
		for row in reader:
			if len(allKeys.keys()) == 0:
				for index, val in enumerate(row):
					allKeys[index] = val

			else:
				for index, val in enumerate(row):
					if allKeys[index] in CSVdict:
						CSVdict[allKeys[index]].append(val)
					else:
						CSVdict[allKeys[index]] = [val]
	return CSVdict

# Remove unwanted keys from the dictionary
def removeUnwantedFields(CSVdict, requiredFields):
	keyToDelete = []
	CSVKeys = CSVdict.keys()
	for key in CSVKeys:
		if key not in requiredFields:
			keyToDelete.append(key)

	for key in keyToDelete:
		del CSVdict[key]
	return CSVdict

# Fetch all trip ids along with arrival time for the given stop id
def fetchTripDataForStop(stop_id):
	try:
		requestedStopID = int(stop_id)
	except:
		requestedStopID = stop_id
		
	# Fetch the arrival times of bus for a given stop_id  
	CSVdict = readFileAndReturnDictionary("./gtfs/stop_times.txt")
	filterIndexes = []
	for index, val in enumerate(CSVdict["stop_id"]):
		if int(val) == requestedStopID:
			filterIndexes.append(index)
	filteredValues = {}
	grid = []
	for key in CSVdict:
		filteredValues[key] = []
		for val in filterIndexes:
			filteredValues[key].append(CSVdict[key][val])
	stopData = removeUnwantedFields(filteredValues, ['trip_id', 'arrival_time', 'departure_time', 'stop_id'])

	# Sort values based on arrival time
	for key in stopData:
		grid.append(stopData[key])
	rez = [[grid[j][i] for j in range(len(grid))] for i in range(len(grid[0]))]
	rez = sorted(rez, key=lambda x:x[list(stopData.keys()).index("arrival_time")])
	grid = [[rez[j][i] for j in range(len(rez))] for i in range(len(rez[0]))]

	# Add empty data for route id and trip headsign
	for index, key in enumerate(stopData.keys()):
		stopData[key] = grid[index]
	stopData["route_id"] = [""] * len(filterIndexes)
	stopData["trip_headsign"] = [""] * len(filterIndexes)

	# Fetch trip details associated with the bus arriving at the given time
	CSVdict = readFileAndReturnDictionary("./gtfs/trips.txt")
	filterIndexes = []
	for index, val in enumerate(CSVdict["trip_id"]):
		if val in stopData['trip_id']:
			filterIndexes.append(index)
	filteredValues = {}
	for key in CSVdict:
		filteredValues[key] = []
		for val in filterIndexes:
			filteredValues[key].append(CSVdict[key][val])
	tripData = removeUnwantedFields(filteredValues, ['trip_id', 'route_id', 'trip_headsign'])

	# Merge the data and return value
	for index, tripId in enumerate(stopData['trip_id']):
		tripDataIndex = tripData['trip_id'].index(tripId)
		stopData["route_id"][index] = tripData['route_id'][tripDataIndex]
		stopData["trip_headsign"][index] = tripData['trip_headsign'][tripDataIndex]

	return stopData

def AllStopsAndCentroid():
	# Fetch locations of all stops and find the center location which will be used to load the map
	CSVdict = readFileAndReturnDictionary('./gtfs/stops.txt')
	northMostPoint = float(max(CSVdict['stop_lat']))
	southMostPoint = float(min(CSVdict['stop_lat']))
	eastMostPoint = float(max(CSVdict['stop_lon']))
	westMostPoint = float(min(CSVdict['stop_lon']))
	centroid = ((northMostPoint + southMostPoint) / 2, (eastMostPoint + westMostPoint) / 2)
	requiredFields = ['stop_id', 'stop_code', 'stop_lat', 'stop_lon', 'stop_name']
	CSVdict = removeUnwantedFields(CSVdict, requiredFields)

	return {
		'centroid' : centroid,
		'allStops' : CSVdict
	}


# Fetch arrival times of bus at the requested stop 
@app.route('/stop/<stop_id>', methods=['GET'])
def fetchData(stop_id):
	data = fetchTripDataForStop(stop_id)
	return jsonify(data), 200

# Fetch all stops and their locations
@app.route('/', methods=['GET'])
def displayMap():
	allStopsAndCentroid = AllStopsAndCentroid()
	return render_template('map.html', centroidOfStops = allStopsAndCentroid['centroid'], allStops = allStopsAndCentroid['allStops'])

if __name__ == '__main__':
   app.run(debug = True)