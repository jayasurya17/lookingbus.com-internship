# Display stops and locations of bus using GTFS data
This application marks all stops under a bus agency. Clicking on a stop gives information about the next 3 busses that will be arriving at the stop. This application also displays the live location of the buses.

### Installation
Install with pip:
> pip install -r requirements.txt

### Running the application
> $python run.py

### Flask application structure
. <br/>
|---------- GTFS/ <br/>
| |-------- agency.txt <br/>
| |-------- calender.txt <br/>
| |-------- calender_dates.txt <br/>
| |-------- fare_attributes.txt <br/>
| |-------- fare_rules.txt <br/>
| |-------- feed_info.txt <br/>
| |-------- routes.txt <br/>
| |-------- shapes.txt <br/>
| |-------- stop_times.txt <br/>
| |-------- stops.txt <br/>
| |-------- trips.txt <br/>
|---------- Templates/ <br/>
| |-------- map.html <br/>
|---------- run.py <br/>
|---------- requirements.txt